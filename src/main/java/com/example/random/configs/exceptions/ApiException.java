package com.example.random.configs.exceptions;

import java.util.ArrayList;
import java.util.List;

public class ApiException extends RuntimeException {
    private List<String> errors = new ArrayList<>();

    public ApiException() {
    }

    public ApiException(String message) {
        super(message);
    }

    public ApiException(List<String> errors) {
        this.errors = errors;
    }

    public ApiException(Throwable cause) {
        super(cause);
    }

    public ApiException(String message, Throwable cause) {
        super(message, cause);
    }

    public List<String> getErrors() {
        return errors;
    }
}
