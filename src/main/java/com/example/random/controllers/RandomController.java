package com.example.random.controllers;

import com.example.random.models.ApiResponse;
import com.example.random.models.Request;
import com.example.random.models.RequestRandom;
import com.example.random.services.RandomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.List;

@RestController
public class RandomController {
    @Autowired
    private RandomService randomService;

    @PostMapping("/random")
    public ApiResponse saveRandom(@RequestBody Request request) {
        List<Integer> randoms = randomService.saveRandom(request);

        return ApiResponse.builder()
                .datetime(LocalDateTime.now().toString())
                .status(HttpStatus.OK.value())
                .message(HttpStatus.OK.getReasonPhrase())
                .data(randoms)
                .build();
    }

    @GetMapping("/random")
    public ApiResponse getRandoms() {
        List<RequestRandom> requestRandoms = randomService.getAll();

        return ApiResponse.builder()
                .datetime(LocalDateTime.now().toString())
                .status(HttpStatus.OK.value())
                .message(HttpStatus.OK.getReasonPhrase())
                .data(requestRandoms)
                .build();
    }
}
