package com.example.random.models;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Data
public class ApiErrorResponse {
    private String datetime;
    private int status;
    private String message;
    private List<String> errors;

    @Builder
    public ApiErrorResponse(String datetime, int status, String message, List<String> errors) {
        this.datetime = datetime;
        this.status = status;
        this.message = message;
        this.errors = errors;
    }
}
