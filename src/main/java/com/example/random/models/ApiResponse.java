package com.example.random.models;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class ApiResponse {
    private String datetime;
    private int status;
    private String message;
    private Object data;

    @Builder
    public ApiResponse(String datetime, int status, String message, Object data) {
        this.datetime = datetime;
        this.status = status;
        this.message = message;
        this.data = data;
    }
}
