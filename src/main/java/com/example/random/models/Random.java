package com.example.random.models;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "rand", catalog = "test")
@Data
@NoArgsConstructor
public class Random {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Integer value;
    private Integer requestId;

//    @ManyToOne
//    private Request request;

    @Builder
    public Random(Integer value, Integer requestId) {
        this.value = value;
        this.requestId = requestId;
    }
}
