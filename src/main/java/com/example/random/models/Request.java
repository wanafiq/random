package com.example.random.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "request", catalog = "test")
@Data
@NoArgsConstructor
public class Request {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Integer id;
    private Integer n;
    private Integer min;
    private Integer max;
    private Double avg;

//    @OneToMany
//    @JoinColumn(name = "request_id")
//    private List<Random> randoms = new ArrayList<>();

    @Builder
    public Request(Integer n, Integer min, Integer max, Double avg) {
        this.n = n;
        this.min = min;
        this.max = max;
        this.avg = avg;
    }
}
