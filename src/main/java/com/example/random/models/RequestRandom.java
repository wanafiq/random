package com.example.random.models;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class RequestRandom {
    private Request request;
    private List<Integer> randoms;

    @Builder
    public RequestRandom(Request request, List<Integer> randoms) {
        this.request = request;
        this.randoms = randoms;
    }
}
