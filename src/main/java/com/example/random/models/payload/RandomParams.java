package com.example.random.models.payload;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class RandomParams {
    private String apiKey;
    private Integer n;
    private Integer min;
    private Integer max;
    private boolean replacement;

    @Builder
    public RandomParams(String apiKey, Integer n, Integer min, Integer max, boolean replacement) {
        this.apiKey = apiKey;
        this.n = n;
        this.min = min;
        this.max = max;
        this.replacement = replacement;
    }
}
