package com.example.random.models.payload;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class RandomRequest {
    private String jsonrpc;
    private String method;
    private RandomParams params;
    private Integer id;

    @Builder
    public RandomRequest(String jsonrpc, String method, RandomParams params, Integer id) {
        this.jsonrpc = jsonrpc;
        this.method = method;
        this.params = params;
        this.id = id;
    }
}
