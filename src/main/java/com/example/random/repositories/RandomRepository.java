package com.example.random.repositories;

import com.example.random.models.Random;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RandomRepository extends JpaRepository<Random, Integer> {


    @Query(value = "select * from test.rand where request_id = ?1", nativeQuery = true)
    List<Integer> findAllByRequestId(int requestId);
}
