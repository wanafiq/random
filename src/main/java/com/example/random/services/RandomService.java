package com.example.random.services;

import com.example.random.configs.exceptions.ApiException;
import com.example.random.models.Random;
import com.example.random.models.Request;
import com.example.random.models.RequestRandom;
import com.example.random.models.payload.RandomParams;
import com.example.random.models.payload.RandomRequest;
import com.example.random.repositories.RandomRepository;
import com.example.random.repositories.RequestRepository;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class RandomService {
    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private RequestRepository requestRepo;

    @Autowired
    private RandomRepository randomRepo;

    public List<RequestRandom> getAll() {
        List<RequestRandom> requestRandoms = new ArrayList<>();

        List<Request> requests = requestRepo.findAll();

        for (Request request : requests) {
            List<Integer> randoms = randomRepo.findAllByRequestId(request.getId());

            RequestRandom requestRandom = RequestRandom.builder()
                    .request(request)
                    .randoms(randoms)
                    .build();

            requestRandoms.add(requestRandom);
        }
        return requestRandoms;
    }

    public List<Integer> saveRandom(Request req) {
        checkInput(req);

        List<Integer> randoms = restCall(req.getN(), req.getMin(), req.getMax());

        if (randoms.size() > 0) {
            // save request
            Request request = Request.builder()
                    .n(req.getN())
                    .min(req.getMin())
                    .max(req.getMax())
                    .build();

            Request savedRequest = requestRepo.save(request);

            // save randoms
            randoms.forEach(r -> {
                Random random = Random.builder()
                        .value(r)
                        .requestId(savedRequest.getId())
                        .build();
                randomRepo.save(random);
            });

            // save average
            List<Integer> randomList = randomRepo.findAllByRequestId(savedRequest.getId());

            savedRequest.setAvg(getAvg(randomList));

            requestRepo.save(request);
        }

        return randoms;
    }

    public List<Integer> restCall(Integer n, Integer min, Integer max) {
        RandomParams params = RandomParams.builder()
                .apiKey("b80c3cbf-b713-4823-b0ec-e296dae866c7")
                .n(n)
                .min(min)
                .max(max)
                .replacement(false)
                .build();

        RandomRequest request = RandomRequest.builder()
                .jsonrpc("2.0")
                .method("generateIntegers")
                .params(params)
                .id(3076)
                .build();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<RandomRequest> requestHttpEntity = new HttpEntity<>(request, headers);

        ResponseEntity<String> responseEntity = restTemplate.postForEntity("https://api.random.org/json-rpc/2/invoke", requestHttpEntity, String.class);

        return parseJson(responseEntity.getBody());
    }

    private List<Integer> parseJson(String json) {
        ObjectMapper mapper = new ObjectMapper();

        List<Integer> randomList = new ArrayList<>();

        try {
            JsonNode rootNode = mapper.readTree(json);
            JsonNode result = rootNode.get("result");
            JsonNode random = result.get("random");
            JsonNode randoms = random.get("data");

            for (JsonNode node : randoms) {
                randomList.add(node.asInt());
            }

        } catch (IOException e) {
            log.error(e.getMessage());
        }

        return randomList;
    }

    private void checkInput(Request req) {
        if (req.getN() < 0) {
            throw new ApiException("value of n must be more than 0");
        }

        if (req.getN() > 3) {
            throw new ApiException("value of n must be less or equal to 3");
        }
    }

    private double getAvg(List<Integer> values) {
        if (values.size() == 0) {
            return 0;
        }

        double sum = 0;
        for (Integer val : values) {
            sum += val;
        }

        return sum / values.size();
    }
}
