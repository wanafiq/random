package com.example.random.services;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RandomServiceTest {
    @Autowired
    private RandomService randomService;

    @Test
    public void restCallTest() {
        int n = 10;
        int min = 1;
        int max = 10;

        List<Integer> randoms = randomService.restCall(n, min, max);

        randoms.forEach(r -> System.out.println(r));

        System.out.println("total : " + randoms.size());
    }

    @Test
    public void parseJsonTest() {
        String json = "{\"jsonrpc\":\"2.0\",\"result\":{\"random\":{\"data\":[2,7,3,5,9,8,10,4,6,1],\"completionTime\":\"2019-05-13 16:14:39Z\"},\"bitsUsed\":33,\"bitsLeft\":249556,\"requestsLeft\":994,\"advisoryDelay\":1300},\"id\":3076}";

        ObjectMapper mapper = new ObjectMapper();

        try {
            JsonNode rootNode = mapper.readTree(json);
            JsonNode result = rootNode.get("result");
            JsonNode random = result.get("random");
            JsonNode randoms = random.get("data");

            for (JsonNode node : randoms) {
                System.out.println(node.asInt());
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
